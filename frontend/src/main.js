import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from "axios";
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
axios.defaults.baseURL = 'http://localhost:5000/api/';
Vue.prototype.$http = axios;

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
