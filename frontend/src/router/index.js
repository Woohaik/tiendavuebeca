import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    name: 'Users',
    component: () => import('../views/Users.vue')
  }
  , {
    path: '/comidas',
    name: 'Foods',
    component: () => import('../views/Foods.vue')
  },
  {
    path: "/perfil/:name",
    name: "Profile",
    component: () => import('../views/Profile.vue')
  },
  {
    path: "*",
    redirect: { name: 'Users' }
  }
]
const router = new VueRouter({
  routes,
  mode: 'history'
})
export default router