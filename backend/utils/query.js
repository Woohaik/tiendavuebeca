module.exports = (res, sql, objt) => {
    require("../config/config").query(sql, objt, (err, response) => {
        if (err) {
            return res.json({
                err,
                status: "bad"
            });
        };
        if (response) {
            return res.json({
                status: "good"
                , response,
                objt
            });
        }
    })
}