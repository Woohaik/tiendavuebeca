//URL base para peticiones
const baseurl = window.location.origin + window.location.pathname;
/*VISTAS PRINCIPALES*/
const vistaUsuarios = document.getElementById("vista-usuarios");
const vistaComidas = document.getElementById("vista-comidas");
//Contendra todas las comidas.
const todasComidasHTML = vistaComidas.getElementsByClassName("todas")[0];

/*VISTAS SECUNDARIAS*/
//VISTA secundaria todos usuarios
const usuariosHTML = document.getElementById("usuarios");
//VISTA secundaria comidas de un usuario
const comidaUsuarioHTML = document.getElementById("usuario");
//Para nombre de usuario seleccionado
const perfilUsuarioHTML = document.getElementById("usuario-perfil");
//Area para mostrar todos los usuarios
const todosUsuariosHTML = usuariosHTML.getElementsByClassName("todos")[0];
//Area para mostrar las comidas de un usuario
const todasComidasUsuarioHTML = comidaUsuarioHTML.getElementsByClassName(
  "comidas"
)[0];

//Navbar
const nav = document.getElementById("nav");
//Usuario seleccionado para mostrar perfil
let selectedUser = null;
//Array que contendra todos los usuarios que hay
let todosUsuarios = [];
//Array que contendra todas las comidas que hay
let todasComidas = [];
//Variable para saber que vista mostrar.
let vistaActual = "a-usuarios";

/*Componentes*/
const boton = (parametroFuncion, funcion, contenido) => {
  return `
    <button type="button" onclick="${funcion}(${parametroFuncion})"> ${contenido} </button>`;
};
/*Cierre Componentes*/

const mostrarTodosUsuarios = () => {
  todosUsuariosHTML.innerHTML = "";
  if (todosUsuarios.length == 0) {
    todosUsuariosHTML.insertAdjacentHTML(
      "beforeend",
      `<div> Aun no hay ningun Usuario registrado </div>`
    );
  } else {
    todosUsuarios.forEach((usuario, index) => {
      todosUsuariosHTML.insertAdjacentHTML(
        "beforeend",
        `<div class="usuario">
                    ${usuario.nombre}
                    ${boton([index], "verComidasUsuario", "Comidas")}
                    ${boton([usuario.id], "borrarUsuario", "Borrar")}   
                </div>`
      );
    });
  }
};

const mostrarTodasComidas = () => {
  todasComidasHTML.innerHTML = "";
  if (todasComidas.length == 0) {
    todasComidasHTML.insertAdjacentHTML(
      "beforeend",
      `<div> Aun no hay ninguna comida registrada</div>`
    );
  } else {
    todasComidas.forEach((comida) => {
      todasComidasHTML.insertAdjacentHTML(
        "beforeend",
        `<div class="comida">
                    ${comida.nombre}
                    ${boton([comida.id], "borrarComida", "Borrar")}   
                </div>`
      );
    });
  }
};

const existeUsuario = (NombreUsuario) => {
  return todosUsuarios.find((usuario) => usuario.nombre == NombreUsuario)
    ? true
    : false;
};

const existeComida = (NombreComida) => {
  return todasComidas.find((comida) => comida.nombre == NombreComida)
    ? true
    : false;
};

const cargarUsuarios = () => {
  let apiRequestUrl = `${baseurl}api/getAllUsers`;
  axios
    .get(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        todosUsuarios = response.data.response;
        mostrarTodosUsuarios();
      }
    })
    .catch((error) => console.log(error));
};

const borrarComida = (id) => {
  let apiRequestUrl = `${baseurl}api/deleteFood/${id}`;
  axios
    .delete(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        cargarComidas();
      }
    })
    .catch((error) => console.log(error));
};

const borrarUsuario = (id) => {
  let apiRequestUrl = `${baseurl}api/deleteUser/${id}`;
  axios
    .delete(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        cargarUsuarios();
      }
    })
    .catch((error) => console.log(error));
};

const cargarComidas = () => {
  let apiRequestUrl = `${baseurl}api/getAllFood`;
  axios
    .get(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        todasComidas = response.data.response;
        mostrarTodasComidas();
      }
    })
    .catch((error) => console.log(error));
};

const agregarComida = (event) => {
  event.preventDefault();
  const inputNuevaComida = document.getElementById("input-nueva-comida");
  if (existeComida(inputNuevaComida.value)) {
    alert("Comida Ya existente");
  } else {
    let apiRequestUrl = `${baseurl}api/addFood/${inputNuevaComida.value}`;
    axios
      .post(apiRequestUrl)
      .then((response) => {
        if (response.data.status == "good") {
          cargarComidas();
          inputNuevaComida.value = "";
        }
      })
      .catch((err) => console.log(error));
  }
};

const agregarUsuario = (event) => {
  event.preventDefault();
  let nuevoUsuariodocument = document.getElementById("new");
  let nuevo = nuevoUsuariodocument.value;
  if (nuevo.length > 0) {
    if (existeUsuario(nuevo)) {
      alert("Usuario Ya existente");
    } else {
      let apiRequestUrl = `${baseurl}api/addUser/${nuevo}`;
      axios
        .post(apiRequestUrl)
        .then((response) => {
          if (response.data.status == "good") {
            cargarUsuarios();
          }
        })
        .catch((err) => console.log(error));
      nuevoUsuariodocument.value = "";
    }
  } else {
    alert("Nombre de Usuario muy pequeño");
  }
};

const toggleView = () => {
  let acomidas = "none";
  let ausuarios = "initial";
  if (vistaComidas.style.display == acomidas) {
    acomidas = "initial";
    ausuarios = "none";
  }
  vistaComidas.style.display = acomidas;
  vistaUsuarios.style.display = ausuarios;
};

const toggleDisplay = () => {
  let aUsuarios = "none";
  let aComida = "initial";
  if (selectedUser == null) {
    aUsuarios = "initial";
    aComida = "none";
  }
  comidaUsuarioHTML.style.display = aComida;
  usuariosHTML.style.display = aUsuarios;
};

const mostrarComidasUsuario = () => {
  todasComidasUsuarioHTML.innerHTML = "";
  if (selectedUser.comidas.length == 0) {
    todasComidasUsuarioHTML.insertAdjacentHTML(
      "beforeend",
      `<div> Este usuario aun no tiene comidas </div>`
    );
  } else {
    selectedUser.comidas.forEach((comida) => {
      todasComidasUsuarioHTML.insertAdjacentHTML(
        "beforeend",
        `<div class="comida">
            ${
              comida.nombre
            } - Cantidad <input class="input" min="1" type="number" value="${parseInt(
          comida.cantidad,
          10
        )}" >    
            <span class="editar">
                  ${boton(
                    [selectedUser.comidas.indexOf(comida)],
                    "editarComida",
                    "Guardar"
                  )}
            </span>
            ${boton(
              [selectedUser.comidas.indexOf(comida)],
              "borrarComidaUsuario",
              "Borrar"
            )}
        </div>`
      );
    });
  }
};

const editarComida = (index) => {
  const input = todasComidasUsuarioHTML
    .getElementsByClassName("comida")
    [index].getElementsByClassName("input")[0];

  if (input.value < 1 || !input.value) {
    input.value = 1;
  }
  let cantidad = parseInt(input.value, 10);
  let userid = selectedUser.id;
  let comidaid = selectedUser.comidas[index].id;

  const objt = {
    cantidad,
  };

  let apiRequestUrl = `${baseurl}api/editFood/${userid}/${comidaid}`;
  axios
    .put(apiRequestUrl, objt)
    .then((response) => {
      if (response.data.status == "good") {
        alert(`Comida de Usuario actualizada nuevo valor = ${cantidad}`);
        cargarComidasUsuario();
      }
    })
    .catch((error) => console.log(error));
};

const borrarComidaUsuario = (index) => {
  let comidaid = selectedUser.comidas[index].id;
  let userid = selectedUser.id;
  let apiRequestUrl = `${baseurl}api/deleteUserFood/${userid}/${comidaid}`;
  axios
    .delete(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        cargarComidasUsuario();
      } else {
        console.log(response);
      }
    })
    .catch((error) => console.log(error));
};

const cargarComidasUsuario = () => {
  let apiRequestUrl = `${baseurl}api/getUserFood/${selectedUser.id}`;
  axios
    .get(apiRequestUrl)
    .then((response) => {
      if (response.data.status == "good") {
        selectedUser.comidas = response.data.response;
        mostrarAgregarComidaUsuario();
        mostrarComidasUsuario();
      } else {
        reject(response);
      }
    })
    .catch((error) => {
      reject(error);
    });
};

const formularioComidaUsuarioReset = () => {
  document.getElementById(
    "formulario-comida-usuario"
  ).innerHTML = ` <label for="comida">comida</label>
  <select id="opciones-agregar-comida"> </select>
  <label for="cantidad">cantindad</label>
  <input  min="1" required id="input-cantidad-comida" type="number" />
  <button type="submit">Agregar</button>`;
};

const mostrarAgregarComidaUsuario = () => {
  formularioComidaUsuarioReset();

  const selectComidaUsuario = document.getElementById(
    "opciones-agregar-comida"
  );
  selectComidaUsuario.innerHTML = "";
  if (todasComidas.length != 0) {
    todasComidas.forEach((comida) => {
      selectComidaUsuario.insertAdjacentHTML(
        "beforeend",
        `<option value="${todasComidas.indexOf(comida)}">${
          comida.nombre
        } </option>`
      );
    });
  } else {
    document.getElementById("formulario-comida-usuario").innerHTML =
      "<p> Aun no hay comidas registradas para agregar a este usuario </p>";
  }
};

const verComidasUsuario = (posicion) => {
  selectedUser = todosUsuarios[posicion];
  selectedUser.comidas = [];
  history.pushState(null, "perfil", `${selectedUser.nombre}`);
  perfilUsuarioHTML.insertAdjacentHTML(
    "afterbegin",
    `<h2>Perfil: ${selectedUser.nombre} </h2>`
  );
  toggleDisplay();
  nav.style.display = "none";
  cargarComidasUsuario();
};

const regresar = () => {
  selectedUser = null;
  nav.style.display = "initial";
  toggleDisplay();
  history.back();
  perfilUsuarioHTML.innerHTML = "";
};

const agregarComidaUsuario = (event) => {
  event.preventDefault();
  const selectComidaUsuario = document.getElementById(
    "opciones-agregar-comida"
  );
  const cantidadComidaUsuario = document.getElementById(
    "input-cantidad-comida"
  );
  if (!cantidadComidaUsuario.value) {
    cantidadComidaUsuario.value = 1;
  }

  let cantidadDeComidaGuarcar = {
    cantidad: parseInt(cantidadComidaUsuario.value, 10),
  };

  let apiRequestUrl = `${baseurl}api/addFoodUser/${selectedUser.id}/${
    todasComidas[selectComidaUsuario.value].id
  }`;
  axios
    .post(apiRequestUrl, cantidadDeComidaGuarcar)
    .then((response) => {
      if (response.data.status == "good") {
        cargarComidasUsuario();
      } else {
        alert("Comida ya agregada anteriormente");
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

const cambiarvista = (event) => {
  const idACambiar = event.target.id;
  if (idACambiar != vistaActual) {
    toggleView();
    event.target.class = "";
    let idVistaActual = document.getElementById(vistaActual);
    idVistaActual.classList = "";
    vistaActual = idACambiar;
    idVistaActual = document.getElementById(vistaActual);
    idVistaActual.classList = "selected";
    history.pushState(
      null,
      `${event.target.innerHTML.toLowerCase()}`,
      `${event.target.innerHTML.toLowerCase()}`
    );
  }
};

const cargarDatos = () => {
  formularioComidaUsuarioReset();
  history.pushState(null, "usuarios", "usuarios");
  cargarUsuarios();
  cargarComidas();
};

window.onload = cargarDatos();
