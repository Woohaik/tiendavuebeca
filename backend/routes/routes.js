const router = require("express").Router();
const query = require("../utils/query");

/*GET*/

router.get("/getAllFood", (req, res) => {
  let sql = "SELECT * FROM comida";
  query(res, sql, null);
});

router.get("/getUserFood/:userid", (req, res) => {
  let usuarioid = req.params.userid;
  let sql =
    "SELECT comida.id , comida.nombre , comida_usuario.cantidad from comida INNER JOIN comida_usuario on comida.id = comida_usuario.comidaid WHERE ?";

  const objt = {
    usuarioid,
  };
  query(res, sql, objt);
});

router.get("/getUserFoods/:username", (req, res) => {
  let nombre = req.params.username;
  let sql = "SELECT id FROM usuario WHERE ?";
  let objt = {
    nombre
  }
  require("../config/config").query(sql, objt, (err, response) => {
    if (err) {
      return res.json({
        err,
        status: "bad"
      });
    };
    if (response[0]) {
      let usuarioid = response[0].id;
      sql =
        "SELECT comida.id , comida.nombre , comida_usuario.cantidad from comida INNER JOIN comida_usuario on comida.id = comida_usuario.comidaid WHERE ?";
      objt = {
        usuarioid,
      };
      query(res, sql, objt);
    } else {
      return res.json({
        status: "bad",
        msg: "usuario no encontrado"
      });
    }
  })


});

router.get("/getAllUsers", (req, res) => {
  let sql = "SELECT * FROM usuario";
  query(res, sql, null);
});

/*POST*/

router.post("/addUser/:nombre", (req, res) => {
  let nombre = req.params.nombre;
  let sql = "INSERT INTO usuario set ?";
  const objt = {
    nombre,
  };
  query(res, sql, objt);
});

router.post("/addFoodUser/:userid/:foodid", (req, res) => {
  const { userid, foodid } = req.params;
  let cantidad = req.body.cantidad;
  if (!cantidad) {
    cantidad = 1;
  }
  const objt = {
    usuarioid: parseInt(userid, 10),
    comidaid: parseInt(foodid, 10),
    cantidad,
  };
  let sql = "INSERT INTO `comida_usuario` set  ? ";
  query(res, sql, objt);
});

router.post("/addFood/:nombre", (req, res) => {
  let nombre = req.params.nombre;
  let sql = "INSERT INTO comida set ?";
  const objt = {
    nombre,
  };
  query(res, sql, objt);
});

/*PUT*/

router.put("/editFood/:userid/:foodid", (req, res) => {
  const { userid, foodid } = req.params;
  let cantidad = req.body.cantidad;
  if (cantidad < 1) {
    cantidad = 1;
  }
  let sql = `UPDATE comida_usuario SET cantidad = ${cantidad} WHERE  usuarioid = ${parseInt(
    userid,
    10
  )} AND comidaid = ${parseInt(foodid, 10)}`;
  query(res, sql, null);
});

/*DELETE*/

router.delete("/deleteUser/:userid", (req, res) => {
  let id = req.params.userid;
  let sql = "DELETE FROM usuario WHERE ?";
  const objt = {
    id,
  };
  query(res, sql, objt);
});

router.delete("/deleteUserFood/:userid/:foodid", (req, res) => {
  let usuarioid = req.params.userid;
  let comidaid = req.params.foodid;
  let sql = `DELETE FROM comida_usuario where usuarioid = ${parseInt(
    usuarioid,
    10
  )} AND comidaid = ${parseInt(comidaid, 10)}`;
  query(res, sql, null);
});

router.delete("/deleteFood/:foodid", (req, res) => {
  let id = req.params.foodid;
  let sql = "DELETE FROM comida WHERE ?";
  const objt = {
    id,
  };
  query(res, sql, objt);
});

module.exports = router;
