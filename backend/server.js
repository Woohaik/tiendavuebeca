const express = require("express");
const app = express();
const history = require("connect-history-api-fallback");
const cors = require('cors');
const connection = require("./config/config");

app.use(history());
//Middlewares
app.use(cors({
  origin: '*'
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//Conexion a base de datos
connection.connect((err) => {
  if (err) throw err;
  console.log("Conectado a base de datos");
});
//API
app.use("/api", require("./routes/routes"));


//Ruta principal
app.use("/", express.static(require("path").join(__dirname, "public")));
//Redireccion a ruta principal
app.get("/*", function (req, res) {
  res.redirect("/");
});
const PORT = 5000;
app.listen(PORT, () => console.log(`Aplicacion en el puerto ${PORT}`));
